package com.fanny.birch.form;

import com.fanny.birch.lib.clustering.BIRCH;
import com.fanny.birch.lib.clustering.BIRCHModify;
import com.fanny.birch.utils.DefaultMutableTreeNode;
import com.fanny.birch.utils.SilhoutteCoeffisien;

import javax.swing.*;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeModel;
import java.awt.*;
import java.util.List;

public class BirchModifyDetail extends JDialog {
    private JPanel treePanel;
    private JTree cfTree;
    private JLabel lblN;
    private JLabel lblPoint;
    private JLabel lblName;
    private JPanel panelPlot;
    private JPanel rootPanel;
    private JLabel lblSilhoutteCoeffisien;

    private BIRCHModify birch;
    private JComponent plot;

    public BirchModifyDetail(Frame frame, BIRCHModify birch, JComponent plot, double[][][] cluster) {
        super(frame);
        this.birch = birch;

        add(rootPanel);
        setSize(1000, 700);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLocationRelativeTo(null);

        setThree();
        lblSilhoutteCoeffisien.setText(Double.toString(SilhoutteCoeffisien.count(cluster)));

        panelPlot.add(plot, BorderLayout.CENTER);
        setTitle("Detail Birch Modify Clustering");

        this.setVisible(true);
    }

    private void setThree() {
        DefaultMutableTreeNode root = new DefaultMutableTreeNode("Root : CF1");
        root.node = birch.getRoot();

        int number = 1;
        for (BIRCH.Node node : birch.getRoot().getChildren()) {
            addThreeNode(root, node, Integer.toString(number));
            number++;
        }

        TreeModel model = new DefaultTreeModel(root);
        cfTree.setModel(model);
        JScrollPane scrollPane = new JScrollPane(cfTree, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED
                , ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        treePanel.add(scrollPane);

        cfTree.getSelectionModel().addTreeSelectionListener(new TreeSelectionListener() {
            @Override
            public void valueChanged(TreeSelectionEvent treeSelectionEvent) {
                DefaultMutableTreeNode node = (DefaultMutableTreeNode) treeSelectionEvent.getPath().getLastPathComponent();
                lblN.setText(Integer.toString(node.node.n));
                lblName.setText(node.getUserObject().toString());
                String point = "";
                for (double p : node.node.sum) {
                    point += p + ", ";
                }
                lblPoint.setText(point);
            }
        });

    }

    private void addThreeNode(DefaultMutableTreeNode root, BIRCH.Node node, String number) {

        if (node != null) {
            int index = 1;
            DefaultMutableTreeNode threeNode = new DefaultMutableTreeNode(String.format("CF%s%d", number, index));
            threeNode.node = node;
            for (BIRCH.Node child : node.getChildren()) {
                addThreeNode(threeNode, child, String.format("%s%d", number, index));

                if (child == null){
                    addThreeLeaf(threeNode, node.leafChildren, String.format("%s%d", number, index));
                }

                index++;
            }

            root.add(threeNode);
        }
    }

    private void addThreeLeaf(DefaultMutableTreeNode root, List<BIRCH.Node> leafChildren, String number){
        int index = 1;
        for(BIRCH.Node child : leafChildren){
            DefaultMutableTreeNode tn = new DefaultMutableTreeNode(String.format("CF%s%d", number, index));
            tn.node = child;
            root.add(tn);
            index++;
        }
    }
}
