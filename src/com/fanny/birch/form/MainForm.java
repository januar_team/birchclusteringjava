package com.fanny.birch.form;

import com.fanny.birch.lib.clustering.BIRCH;
import com.fanny.birch.lib.clustering.BIRCHModify;
import smile.clustering.Clustering;
import smile.data.AttributeDataset;
import smile.data.parser.DelimitedTextParser;
import smile.plot.Palette;
import smile.plot.PlotCanvas;
import smile.plot.ScatterPlot;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;

public class MainForm extends JFrame implements ActionListener, Runnable {
    private static final String ERROR = "Error";

    private JPanel rootPanel;
    private JButton btnStart;
    private JTextField txtK;
    private JTextField txtB;
    private JTextField txtT;
    private JPanel panelPlot;
    private JPanel panelPlotBirch;
    private JPanel panelPlotModifBirch;
    private JLabel labelBirch;
    private JLabel labelBirchModify;
    private JButton btnBIRCHDetail;
    private JButton btnBIRCHModifyDetail;
    private JComponent plotBirch;
    private JComponent plotModBirch;

    private double[][] dataset;
    int clusterNumber = 2;
    int B = 2;
    double T = 0.1;
    int minPts = 2;
    char pointLegend = '.';

    BIRCH birch;
    BIRCHModify birchModify;

    double[][][] clusterBirch;
    double[][][] clusterBirchModify;

    public MainForm() {
        add(rootPanel);

        setTitle("BIRCH Clustering - Fanny");
        setSize(1000, 700);

        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        txtK.setText(Integer.toString(clusterNumber));
        txtB.setText(Integer.toString(B));
        txtT.setText(Double.toString(T));


        setLocation();
        setData();

        btnStart.setActionCommand("startButton");
        btnStart.addActionListener(this);

        btnBIRCHDetail.setActionCommand("viewDetailBirch");
        btnBIRCHDetail.addActionListener(this);

        btnBIRCHModifyDetail.setActionCommand("viewDetailBirchModify");
        btnBIRCHModifyDetail.addActionListener(this);
    }

    private void setLocation() {
        /*Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        int height = dim.height;
        int width = dim.width;
        this.setSize(width/2, height/2);*/

        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }

    private void setData() {
        String path = System.getProperty("user.dir", "");
        try {
            path = path + "/data.csv";
            File file = new File(path);

            DelimitedTextParser parser = new DelimitedTextParser();
            parser.setDelimiter(",");
            AttributeDataset data = parser.parse("Online Retail Normalization", file);
            dataset = data.toArray(new double[data.size()][]);
            System.out.println(file.getAbsolutePath());
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Failed to load dataset.", "ERROR", JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace();
        }

        char pointLegend = '.';
        if (dataset.length < 500) {
            pointLegend = 'o';
        }

        plotBirch = ScatterPlot.plot(dataset, pointLegend);
        panelPlotBirch.setVisible(true);
        panelPlotBirch.add(plotBirch, BorderLayout.CENTER);
        panelPlotBirch.validate();

        plotModBirch = ScatterPlot.plot(dataset, pointLegend);
        panelPlotModifBirch.setVisible(true);
        panelPlotModifBirch.add(plotModBirch, BorderLayout.CENTER);
        panelPlotModifBirch.validate();
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        if (actionEvent.getActionCommand().equals("startButton")) {
            try {
                clusterNumber = Integer.parseInt(txtK.getText().trim());
                if (clusterNumber < 2) {
                    JOptionPane.showMessageDialog(this, "Invalid K: " + clusterNumber, "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }

                if (clusterNumber > dataset.length / 2) {
                    JOptionPane.showMessageDialog(this, "Too large K: " + clusterNumber, "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(this, "Invalid K: " + txtK.getText(), "Error", JOptionPane.ERROR_MESSAGE);
                return;
            }

            Thread thread = new Thread(this);
            thread.start();
        }else if(actionEvent.getActionCommand().equals("viewDetailBirch")){
            if (birch == null){
                JOptionPane.showMessageDialog(this, "Please start clustering before view detail!",
                        "Error", JOptionPane.ERROR_MESSAGE);
            }else{
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        BirchDetail birchDetail = new BirchDetail(MainForm.this, birch, plotBirch, clusterBirch);
                    }
                });
            }
        }else if(actionEvent.getActionCommand().equals("viewDetailBirchModify")){
            if (birch == null){
                JOptionPane.showMessageDialog(this, "Please start clustering before view detail!",
                        "Error", JOptionPane.ERROR_MESSAGE);
            }else{
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        new BirchModifyDetail(MainForm.this, birchModify, plotModBirch, clusterBirchModify);
                    }
                });
            }
        }
    }

    @Override
    public void run() {
        btnStart.setEnabled(false);
        if (validateForm()) {
            JComponent plot = learnBirch();
            if (plot != null) {
                panelPlotBirch.remove(plotBirch);
                plotBirch = plot;
                panelPlotBirch.add(plotBirch, BorderLayout.CENTER);
            }

            JComponent plotModify = learnBirchModify();
            if (plotModify != null) {
                panelPlotModifBirch.remove(plotModBirch);
                plotModBirch = plotModify;
                panelPlotModifBirch.add(plotModBirch, BorderLayout.CENTER);
            }
        }
        panelPlotBirch.validate();
        panelPlotModifBirch.validate();

        btnStart.setEnabled(true);
    }

    private boolean validateForm() {
        try {
            B = Integer.parseInt(txtB.getText().trim());
            if (B < 2) {
                JOptionPane.showMessageDialog(this, "Invalid B: " + B, ERROR, JOptionPane.ERROR_MESSAGE);
                return false;
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Invalid B: " + txtB.getText(), ERROR, JOptionPane.ERROR_MESSAGE);
            return false;
        }

        try {
            T = Double.parseDouble(txtT.getText().trim());
            if (T <= 0) {
                JOptionPane.showMessageDialog(this, "Invalid T: " + T, ERROR, JOptionPane.ERROR_MESSAGE);
                return false;
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Invalid T: " + txtT.getText(), ERROR, JOptionPane.ERROR_MESSAGE);
            return false;
        }

        return true;
    }

    private JComponent learnBirch() {
        long clock = System.currentTimeMillis();
        birch = new BIRCH(2, B, T);
        for (int i = 0; i < dataset.length; i++)
            birch.add(dataset[i]);

        if (birch.partition(clusterNumber, minPts) < clusterNumber) {
            JOptionPane.showMessageDialog(this, "The number of non-outlier leaves is less than " + clusterNumber + ". Try larger T.", "ERROR", JOptionPane.ERROR_MESSAGE);
            return null;
        }

        int[] membership = new int[dataset.length];
        int[] clusterSize = new int[clusterNumber];
        for (int i = 0; i < dataset.length; i++) {
            membership[i] = birch.predict(dataset[i]);
            if (membership[i] != Clustering.OUTLIER) {
                clusterSize[membership[i]]++;
            }
        }

        int[] count = birch.getSumLeaf();

        labelBirch.setText(String.format("<html>BIRCH clusterings %d samples in %dms " +
                        "<br/>Total Non Leaf : %d" +
                        "<br/>Total Leaf : %d</html>",
                dataset.length, System.currentTimeMillis() - clock,
                count[0], count[1]));

        PlotCanvas plot = ScatterPlot.plot(birch.centroids(), '@');
        plot.getBase();
        clusterBirch = new double[clusterNumber][][];
        for (int k = 0; k < clusterNumber; k++) {
            if (clusterSize[k] > 0) {
                clusterBirch[k] = new double[clusterSize[k]][];
                for (int i = 0, j = 0; i < dataset.length; i++) {
                    if (membership[i] == k) {
                        clusterBirch[k][j++] = dataset[i];
                    }
                }

                plot.points(clusterBirch[k], pointLegend, Palette.COLORS[k % Palette.COLORS.length]);
            }
        }
        plot.points(birch.centroids(), '@');
        return plot;
    }

    private JComponent learnBirchModify() {
        long clock = System.currentTimeMillis();
        birchModify = new BIRCHModify(2, B, T);
        for (int i = 0; i < dataset.length; i++)
            birchModify.add(dataset[i]);

        if (birchModify.partition(clusterNumber, minPts) < clusterNumber) {
            JOptionPane.showMessageDialog(this, "The number of non-outlier leaves is less than " + clusterNumber + ". Try larger T.", "ERROR", JOptionPane.ERROR_MESSAGE);
            return null;
        }

        int[] membership = new int[dataset.length];
        int[] clusterSize = new int[clusterNumber];
        for (int i = 0; i < dataset.length; i++) {
            membership[i] = birchModify.predict(dataset[i]);
            if (membership[i] != Clustering.OUTLIER) {
                clusterSize[membership[i]]++;
            }
        }


        int[] count = birchModify.getSumLeaf();

        labelBirchModify.setText(String.format("<html>BIRCH Modify clusterings %d samples in %dms" +
                        "<br/>Total Non Leaf : %d" +
                        "<br/>Total Leaf : %d</html>", dataset.length, System.currentTimeMillis() - clock,
                count[0], count[1]));

        PlotCanvas plot = ScatterPlot.plot(birchModify.centroids(), '@');
        plot.getBase();
        clusterBirchModify = new double[clusterNumber][][];
        for (int k = 0; k < clusterNumber; k++) {
            if (clusterSize[k] > 0) {
                clusterBirchModify[k] = new double[clusterSize[k]][];
                for (int i = 0, j = 0; i < dataset.length; i++) {
                    if (membership[i] == k) {
                        clusterBirchModify[k][j++] = dataset[i];
                    }
                }

                plot.points(clusterBirchModify[k], pointLegend, Palette.COLORS[k % Palette.COLORS.length]);
            }
        }
        plot.points(birchModify.centroids(), '@');
        return plot;
    }
}
