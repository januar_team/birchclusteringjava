package com.fanny.birch.lib.clustering;


public class BIRCHModify extends BIRCH {
    /**
     * Constructor.
     *
     * @param d the dimensionality of data.
     * @param B the branching factor. Maximum number of children nodes.
     * @param T the maximum radius of a sub-cluster.
     */
    public BIRCHModify(int d, int B, double T) {
        super(d, B, T);
    }

    /**
     * Add a data point into CF tree.
     */
    @Override
    public void add(double[] x) {
        if (root == null) {
            root = new BIRCHModify.Node();
            root.add(new BIRCHModify.Leaf(x));
            root.update(x);
        } else {
            root.add(x);
        }
    }


    public class Node extends BIRCH.Node{

        public Node(){
            super();
        }

        /**
         * Adds data to the node.
         */
        @Override
        void add(double[] x) {
            update(x);

            int index = 0;
            double smallest = children[0].distance(x);

            // find the closest child node to this data point
            for (int i = 1; i < numChildren; i++) {
                double dist = children[i].distance(x);
                if (dist < smallest) {
                    index = i;
                    smallest = dist;
                }
            }

            if (children[index] instanceof BIRCHModify.Leaf) {
                if (smallest > T) {
                    // increase T and check again
                    double temp = T * 2;
                    if (smallest > temp){
                        add(new BIRCHModify.Leaf(x));
                    }else{
                        T = temp;
                        children[index].add(x);
                        children[index].leafChildren.add(new BIRCHModify.Leaf(x));
                    }
                } else {
                    children[index].add(x);
                    children[index].leafChildren.add(new BIRCHModify.Leaf(x));
                }
            } else {
                children[index].add(x);
                children[index].leafChildren.add(new BIRCHModify.Leaf(x));
            }
        }


        /**
         * Add a node as children. Split this node if the number of children
         * reach the Branch Factor.
         */
        @Override
        void add(BIRCH.Node node) {
            if (numChildren < B) {
                children[numChildren++] = node;
                node.parent = this;
            }
            /*else
                {
                if (parent == null) {
                    parent = new BIRCHModify.Node();
                    parent.add(this);
                    root = parent;
                } else {
                    parent.n = 0;
                    Arrays.fill(parent.sum, 0.0);
                }

                parent.add(split(node));

                for (int i = 0; i < parent.numChildren; i++) {
                    parent.n += parent.children[i].n;
                    for (int j = 0; j < d; j++) {
                        parent.sum[j] += parent.children[i].sum[j];
                    }
                }
            }*/
        }
    }

    /**
     * Leaf node of CF tree.
     */
    class Leaf extends BIRCH.Leaf {

        /**
         * Constructor.
         */
        Leaf(double[] x) {
            super(x);
        }
    }

    public int[] getSumLeaf(){
        int[] count = new int[2];

        if (root != null){
            count[0]++;
            for (int i = 0; i < root.children.length; i++){
                if (root.children[i] instanceof BIRCH.Node){
                    count[0]++;
                    for (BIRCH.Node n : root.children[i].leafChildren){
                        count[1]++;
                    }
                }else{
                    count[1]++;
                }
            }
        }

        return count;
    }
}
