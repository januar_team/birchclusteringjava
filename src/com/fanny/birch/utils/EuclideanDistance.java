package com.fanny.birch.utils;

public class EuclideanDistance {
    public static double count(double[] x, double[] y){
        double distance = 0.0;

        for (int i = 0; i < x.length; i++){
            distance += Math.pow(x[i] - y[i], 2);
        }

        return Math.sqrt(distance);
    }
}
