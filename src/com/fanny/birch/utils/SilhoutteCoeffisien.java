package com.fanny.birch.utils;

public class SilhoutteCoeffisien {

    public static double count(double[][][] cluster){
        double result = 0.0;

        for (int i = 0 ; i < cluster.length; i++){
            double a = 0.0;
            double b = 999999999999.0;
            int count = 0;
            for (int j = 0; j < cluster[i].length; j++){
                for (int k = j + 1; k < cluster[i].length; k++){
                    a += EuclideanDistance.count(cluster[i][j], cluster[i][k]);
                    count ++;
                }

                for (int k = 0; k < cluster.length; k++){
                    if (k == i) continue;

                    int countB = 0;
                    double tempB = 0;
                    for (int l = 0; l < cluster[k].length; l++){
                        tempB += EuclideanDistance.count(cluster[i][j], cluster[k][l]);
                        countB++;
                    }
                    tempB = tempB / countB;

                    b = (tempB < b) ? tempB : b;
                }
            }
            a = a / count;

            result += (1 - (a /b));
        }

        return result / cluster.length;
    }
}
