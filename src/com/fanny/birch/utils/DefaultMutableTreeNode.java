package com.fanny.birch.utils;

import com.fanny.birch.lib.clustering.BIRCH;

public class DefaultMutableTreeNode extends javax.swing.tree.DefaultMutableTreeNode {
    public BIRCH.Node node;

    public DefaultMutableTreeNode(String title){
        super(title);
    }
}
